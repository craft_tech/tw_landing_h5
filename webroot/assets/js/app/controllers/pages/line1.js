// Page module
define(["app"],

    function(app) {

        var Page = {};

        Page.View = Backbone.Layout.extend({

            mainTimeline: null,
            beforeRender: function() {

                var done = this.async();
                done();

            },

            afterRender: function() {
                var context = this;

                // var line_animation1 = new TimelineMax();
                // line_animation1
                //     .from(context.$(".main"), 8, { rotation: -90, ease: Power0.easeNone }, 1)
                //     .from(context.$(".yun"), 8, { rotation: 90, ease: Power0.easeNone }, 1)
                // var plane_animation = new TimelineMax({ repeat: -1 });
                // plane_animation
                //     .to(context.$(".plane"), 5, { left: "-=5%", top: "+=20%", ease: Power0.easeNone }, 1)
                //     .to(context.$(".plane"), 5, { left: "-=10%", top: "+=15%", ease: Power0.easeNone })
                // var jiangluosan_animation = new TimelineMax({ repeat: -1 });
                // jiangluosan_animation
                //     .to(context.$(".jiangluosan"), 10, { left: "-=5%", top: "+=10%", ease: Power0.easeNone }, 1)
                //     .to(context.$(".jiangluosan"), 10, { left: "-=5%", top: "-=8%", ease: Power0.easeNone })
                //     .to(context.$(".jiangluosan"), 10, { left: "+=10%", top: "-=2%", ease: Power0.easeNone })
                // var animation0 = new TimelineMax({ repeat: -1 });
                // animation0
                //     .to(context.$(".bird_1"), 10, { left: "+=10%", top: "-=15%", ease: Power0.easeNone }, 1)
                //     .to(context.$(".bird_2"), 10, { left: "+=10%", top: "-=15%", ease: Power0.easeNone }, 1)
                //     .to(context.$(".bird_3"), 10, { left: "+=7%", top: "-=10%", ease: Power0.easeNone }, 1)
                //     .to(context.$(".bird2"), 5, { left: "+=7%", top: "-=10%", ease: Power0.easeNone }, 1)
                //     .to(context.$(".reqiqiu1"), 10, { left: "+=7%", top: "+=10%", ease: Power0.easeNone }, 1)
                //     .to(context.$(".reqiqiu2"), 10, { left: "-=7%", top: "+=10%", ease: Power0.easeNone }, 1)
            },

            loopAnimation: function() {}

        });

        // Return the module for AMD compliance.
        return Page;

    });