// Page module
define(["app"],

    function (app) {

        var Page = {};

        Page.View = Backbone.Layout.extend({

            mainTimeline: null,
            beforeRender: function () {
                var done = this.async();
                done();

            },

            afterRender: function () { 
                var context = this;
                

                    $(".next_longmuExpand").bind("click", function () { 
                        $(".longmushan_bg").css("display", "none");
                        $(".map_box").css("display", "block");
                        $(".longmuExpand").css("display", "none");   
                        $(".longmudao_nav").css("background-color","orange");
                         $(".longmudao_nav").css("color","white");
                    })
                 
                    $(".next_map_box").bind("click", function () { 
                        $(".longmushan_bg").css("display", "block");
                        $(".map_box").css("display", "none");
                        $(".longmuExpand").css("display", "none");  
                        $(".mindandao_nav").css("background-color","orange");
                         $(".mindandao_nav").css("color","white");
                    })
                
                    $(".next_longmushan").bind("click", function () { 
                        $(".longmushan_bg").css("display", "none");
                        $(".map_box").css("display", "none");
                        $(".longmuExpand").css("display", "block");  
                        $(".suwu_nav").css("background-color","orange")
                         $(".suwu_nav").css("color","white");
                    })

                     $(".pre_longmuExpand").bind("click", function () { 
                        $(".longmushan_bg").css("display", "block");
                        $(".map_box").css("display", "none");
                        $(".longmuExpand").css("display", "none");   
                    })
                 
                    $(".pre_map_box").bind("click", function () { 
                        $(".longmushan_bg").css("display", "none");
                        $(".map_box").css("display", "none");
                        $(".longmuExpand").css("display", "block");  
                    })
                
                    $(".pre_longmushan").bind("click", function () { 
                        $(".longmushan_bg").css("display", "none");
                        $(".map_box").css("display", "block");
                        $(".longmuExpand").css("display", "none");  
                    })
                

                console.log("line3 afterRender");
            },

            loopAnimation: function () {

            }

        });

        // Return the module for AMD compliance.
        return Page;

    });
