define(["controllers/pages/home",
    "controllers/pages/mobile"
],


    function (Home, Mobile) {


       
        var flag = true;
        var pcMobileFlag = IsPC();
        function IsPC() {
            var userAgentInfo = navigator.userAgent;
            var Agents = ["Android", "iPhone",
                "SymbianOS", "Windows Phone",
                "iPad", "iPod"];
            // var flag = true;
            for (var v = 0; v < Agents.length; v++) {
                if (userAgentInfo.indexOf(Agents[v]) > 0) {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        if (!flag) {
            //手机端 
            console.log("手机端");
             var pages = [{
            routeId: 'home',
            type: 'main',
            landing: false,
            page: function () {
                return new Home.View({ template: "pages/home" });
            }
        },
        {
            routeId: 'mobile',
            type: 'main',
            landing: true,
            page: function () {
                return new Mobile.View({ template: "pages/mobile" });
            }
        }


        ];
        }
        else {
            //pc端
            console.log("PC端");
            var pages = [{
                routeId: 'home',
                type: 'main',
                landing: true,
                page: function () {
                    return new Home.View({ template: "pages/home" });
                }
            },
            {
                routeId: 'mobile',
                type: 'main',
                landing: false,
                page: function () {
                    return new Mobile.View({ template: "pages/mobile" });
                }
            }


            ];
        }




        return pages;

    });