/* global module */
module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        requirejs: {
            default: {
                options: {
                    name: "main",
                    wrapShim: true,
                    baseUrl: "../webroot/assets/js/app",
                    mainConfigFile: "../webroot/assets/js/app/config/config.js",
                    out: "../webroot/assets/js/_app.js",
                    insertRequire: ['main'],
                    preserveLicenseComments: false
                }
            }

        },

        jst: {
            default: {
                options: {
                    templateSettings: {
                        interpolate: /\{\{(.+?)\}\}/g
                    },
                    prettify: true,
                    processName: function (filepath) {

                        var prefix = '../webroot/assets/js/app/views/';
                        var suffix = '.html';

                        if (filepath.indexOf(prefix) == 0) {
                            filepath = filepath.substring(prefix.length);
                        }

                        if (filepath.lastIndexOf(suffix) == filepath.length - suffix.length) {
                            filepath = filepath.substring(0, filepath.length - suffix.length);
                        }

                        return filepath;
                    }
                },
                files: {
                    "../webroot/assets/js/_views.js": ["../webroot/assets/js/app/views/**/*.html"]
                }
            }
        },

        concat: {
            options: {
                separator: ';\n\n'
            },
            dist: {
                src: ['../webroot/assets/js/app/vendor/base/require.js', '../webroot/assets/js/_views.js', '../webroot/assets/js/_app.js'],
                dest: '../webroot/assets/js/app.js'
            }
        },

        less: {
            default: {
                options: {
                    //paths: ["assets/css"],
                    cleancss: true,
                    modifyVars: {
                        //imgPath: '"http://mycdn.com/path/to/images"',
                        //bgColor: 'red'
                    }
                },
                files: {
                    "../webroot/assets/css/style.css": "../webroot/assets/css/style.less"
                }
            }
        },

        clean: {
            options: { force: true },
            default: ["../webroot/assets/js/_app.js", "../webroot/assets/js/_views.js"]
        },


        tmtTinyPng: {
            default_options: {
                options: {
                },
                files: [
                    {
                        expand: true,
                        src: ['*.png','*.jpg','*/*.png','*/*.jpg'],
                        cwd: '../webroot/assets/images',
                        filter: 'isFile'
                    }
                ]
            }
        }


    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jst');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-css-url-embed');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-tmt-tiny-png');


    grunt.registerTask('default', ['requirejs', 'jst', 'concat', 'less', 'clean']);

};
