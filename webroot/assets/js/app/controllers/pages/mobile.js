// Page module
define(["app", "controllers/base/page", "controllers/pages/page1", "controllers/pages/page2",
    "controllers/pages/page3", "controllers/pages/page4", "controllers/pages/page5"
],

    function (app, BasePage, Page1, Page2, Page3, Page4, Page5) {

        var Page = {};
        var currentNowPage = "";
        Page.View = BasePage.View.extend({
            scroll: null,
            beforeRender: function () {
                var done = this.async();
                require(["vendor/ui/iscroll"],
                    function () {
                        done();
                    });
            },

            afterRender: function () {



                // 字体自适应开始
                ;
                (function (designWidth, maxWidth) {
                    var doc = document,
                        win = window;
                    var docEl = doc.documentElement;
                    var tid;
                    var rootItem, rootStyle;

                    function refreshRem() {
                        var width = docEl.getBoundingClientRect().width;
                        if (!maxWidth) {
                            maxWidth = 540;
                        };
                        if (width > maxWidth) {
                            width = maxWidth;
                        }
                        //与淘宝做法不同，直接采用简单的rem换算方法1rem=100px
                        var rem = width * 16 / designWidth;
                        //兼容UC开始
                        rootStyle = "html{font-size:" + rem + 'px !important}';
                        rootItem = document.getElementById('rootsize') || document.createElement("style");
                        if (!document.getElementById('rootsize')) {
                            document.getElementsByTagName("head")[0].appendChild(rootItem);
                            rootItem.id = 'rootsize';
                        }
                        if (rootItem.styleSheet) {
                            rootItem.styleSheet.disabled || (rootItem.styleSheet.cssText = rootStyle)
                        } else {
                            try { rootItem.innerHTML = rootStyle } catch (f) { rootItem.innerText = rootStyle }
                        }
                        //兼容UC结束
                        docEl.style.fontSize = rem + "px";
                        console.log(1);
                    };
                    refreshRem();

                    win.addEventListener("resize", function () {
                        clearTimeout(tid); //防止执行两次
                        tid = setTimeout(refreshRem, 100);
                    }, false);

                    win.addEventListener("pageshow", function (e) {
                        if (e.persisted) { // 浏览器后退的时候重新计算
                            clearTimeout(tid);
                            tid = setTimeout(refreshRem, 100);
                        }
                    }, false);

                    if (doc.readyState === "complete") {
                        doc.body.style.fontSize = "16px";
                    } else {
                        doc.addEventListener("DOMContentLoaded", function (e) {
                            doc.body.style.fontSize = "16px";
                        }, false);
                    }
                })(750, 6400);
                // 字体自适应结束




                // isertView : https://github.com/tbranyen/backbone.layoutmanager/wiki/Example-usage
                //http://www.ruanyifeng.com/blog/2011/08/a_detailed_explanation_of_jquery_deferred_object.html
                var page1 = this.insertView("#pageContent1", new Page1.View({ template: "pages/page1" }));
                var page2 = this.insertView("#pageContent2", new Page2.View({ template: "pages/page2" }));
                var page3 = this.insertView("#pageContent3", new Page3.View({ template: "pages/page3" }));
                var page4 = this.insertView("#pageContent4", new Page4.View({ template: "pages/page4" }));
                var page5 = this.insertView("#pageContent5", new Page5.View({ template: "pages/page5" }));
                var startDfd = $.Deferred();
                startDfd
                    .then($.proxy(page1.render, page1))
                    .then($.proxy(page2.render, page2))
                    .then($.proxy(page3.render, page3))
                    .then($.proxy(page4.render, page4))
                    .then($.proxy(page5.render, page5))
                    .then($.proxy(this.init, this));
                startDfd.resolve();
            },

            init: function () {
                //init iscroll
                var content = this;
                content.scroll = new IScroll(content.$("#wrapperMobile").get(0), {
                    disablePointer: true,
                    mouseWheel: true,
                    snap: '.page',
                    momentum: false,
                    hScrollbar: false,
                    bounceTime: '600'
                });

                function isPassive() {
                    var supportsPassiveOption = false;
                    try {
                        addEventListener("test", null, Object.defineProperty({}, 'passive', {
                            get: function () {
                                supportsPassiveOption = true;
                            }
                        }));
                    } catch (e) { }
                    return supportsPassiveOption;
                }

                document.addEventListener('touchmove', function (e) {
                    e.preventDefault();
                }, isPassive() ? {
                    capture: false,
                    passive: false
                } : false);
                //currentPage event
                content.scroll.on('scrollEnd', doSomething);

                function doSomething() {
                    console.log("当前是第" + content.scroll.currentPage.pageY + "页");
                    if (content.scroll.currentPage.pageY >= 0) {
                        currentNowPage = content.scroll.currentPage.pageY;
                    }
                }
            },
            resize: function (ww, wh, orient) {
                //如果第二页竖屏，则提示用户啊横屏
                var pageNow = currentNowPage;
                if(currentNowPage){ 
                        if (orient == "landscape" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "portrait") {
                            // $(".rotate-portrait").show();
                            // $(".rotate-landscape").hide();
                        }
                        else if (orient == "portrait" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "portrait") {
                            // $(".rotate-landscape").show();
                            // $(".rotate-portrait").hide();
                        }
                        else {
                            // $(".rotate-portrait").hide();
                            // $(".rotate-landscape").hide();
                        }
                }
 
            },
        });

        // Return the module for AMD compliance.
        return Page;

    });