// Page module
define(["app"],

    function (app) {

        var Page = {};

        Page.View = Backbone.Layout.extend({

			mainTimeline : null, 
            beforeRender: function () {

                var done = this.async(); 
                    done(); 
                
            },
 
            afterRender: function () { 
				var context = this;  
 
                console.log("line2 afterRender");
                function trackLocation(e) {
                    var pX = e.pageX+50;
                    var rect = videoContainer.getBoundingClientRect(),
                        position = ((pX - rect.left) / videoContainer.offsetWidth)*100;
                    if (position <= 100) {
                        videoClipper.style.width = position+"%";
                        clippedVideo.style.width = ((100/position)*100)+"%";
                        clippedVideo.style.zIndex = 3;

                    }
                    if(position >= 95) {
                        $('#video-clipper').removeClass('video-cli-ant');
                    }else{
                        $('#video-clipper').addClass('video-cli-ant');
                    }

                }
                var videoContainer = document.getElementById("video-compare-container"),
                    videoClipper = document.getElementById("video-clipper"),
                    clippedVideo = videoClipper.getElementsByTagName("video")[0];
                    videoContainer.addEventListener( "mousemove", trackLocation, false);
                    videoContainer.addEventListener("touchstart",trackLocation,false);
                    videoContainer.addEventListener("touchmove",trackLocation,false);
            },
			
			loopAnimation : function() { 
  
			}
  
        });

        // Return the module for AMD compliance.
        return Page;

    });
