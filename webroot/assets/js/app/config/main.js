require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker"],


    function (app, LayoutManager, Router, ResizeManager, Tracker) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        app.tracker = new Tracker({gaAccount : "", baiduAccount : ""});

        app.eventBus = _.extend({}, Backbone.Events);
		
        app.sound1 = new Howl({
			urls: [app.assetsPath + 'audio/environment.mp3'],
			loop : true,
			sprite: {
				complete: [0, 10000],
				
			}
		});
		
		app.sound3 = new Howl({
			urls: [app.assetsPath + 'audio/shake.mp3'],
			loop : false,
			sprite: {
				complete: [0, 3000],
				
			}
		});
		//app.sound1.stop();
	
		app.sound2 = new Howl({
			urls: [app.assetsPath + 'audio/monkey.mp3'],
			loop : false,
			sprite: {
				complete: [0, 58000]
				
			}
		});
		//app.sound2.stop();

		//app.sound.fade(0, 1, 4000, null, "complete");

		app.isSoundPlaying = false;

        /*
         app.user = new User.Model();
         app.user.getInfo();
         */

        app.layout.render().promise().done(function () {
            Backbone.history.start({
                pushState: false
            });
        });
		
		document.addEventListener('touchmove',function(e){
                    
                        e.preventDefault();
						e.stopPropagation();
                     
                }, false);


    });