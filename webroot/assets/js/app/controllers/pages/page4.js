// Page module
define(["app"],

    function (app) {

        var Page = {};

        Page.View = Backbone.Layout.extend({

			mainTimeline : null, 
            beforeRender: function () {

                var done = this.async(); 
                    done(); 
                
            },
 
            afterRender: function () { 
				var context = this;  
 
                console.log("page4 afterRender");
            },
			
			loopAnimation : function() { 
  
			}
  
        });

        // Return the module for AMD compliance.
        return Page;

    });
