// Layout module
define(["app", "controllers/base/layout"],

    function (app, BaseLayout) {

        var Layout = {};

        Layout.View = BaseLayout.View.extend({


            el: "#main",
            template: "layouts/main",


            beforeRender: function () {

                var done = this.async();
                done();

            },



            afterRender: function () {


            },


            resize: function (ww, wh, orient) {
                var flag = true;
                //手机，pc
                if (app.router.pageHolders.length > 0) {
                    
                    function IsPC() {
                        var userAgentInfo = navigator.userAgent;
                        var Agents = ["Android", "iPhone",
                            "SymbianOS", "Windows Phone",
                            "iPad", "iPod"];
                        // var flag = true;
                        for (var v = 0; v < Agents.length; v++) {
                            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                                flag = false;
                                break;
                            }
                        }
                        return flag;
                    }
                    var pMoble = IsPC();
                    if (!flag) {
                        //手机端需在第二页判断横屏显示
                        console.log("手机端");
                            if (orient == "landscape" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "portrait") {
                                // this.$(".rotate-portrait").show();
                                // this.$(".rotate-landscape").hide();
                            }
                            else if (orient == "portrait" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "landscape") {
                                // this.$(".rotate-landscape").show();
                                // this.$(".rotate-portrait").hide();
                            }
                            else {
                                // this.$(".rotate-portrait").hide();
                                // this.$(".rotate-landscape").hide();
                            }

                    }
                    else {
                        console.log("pc端");
                            if (orient == "landscape" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "portrait") {
                                // this.$(".rotate-portrait").show();
                                // this.$(".rotate-landscape").hide();
                            }
                            else if (orient == "portrait" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "landscape") {
                                // this.$(".rotate-landscape").show();
                                // this.$(".rotate-portrait").hide();
                            }
                            else {
                                // this.$(".rotate-portrait").hide();
                                // this.$(".rotate-landscape").hide();
                            }
                    }
  
                } 
            }


        });

        // Return the module for AMD compliance.
        return Layout;

    });
